#include "MemoryManager.h"
#include <string.h>
#include <sstream>

template <class T>
std::string ToString(const T& t)
{
    std::ostringstream sstr;
    sstr << t;
    return sstr.str();
}


CMemoryManager::CMemoryManager()
{

}

CMemoryManager::~CMemoryManager()
{

}




// If preallocatedBuffer is 0 (NULL) MM should allocate numBytes memory
// Otherwise MM should use the provided preallocatedBuffer
bool CMemoryManager::Initialise(size_t numBytes, void *preallocatedBuffer)
{
    m_totalMemory=numBytes;
    m_memoryStartAddress=(BYTE*)preallocatedBuffer;
    if (m_memoryStartAddress==0)
    {
        m_memoryStartAddress=new BYTE[numBytes];

        if (!m_memoryStartAddress)
        {
            //AddStatusText("Could not allocate memory of: "+ToString(numBytes)+" bytes.");
            return false;
        }
    }

    m_nextFreeMemoryAddress=m_memoryStartAddress;

    return true;
}




void* CMemoryManager::Allocate(size_t numBytes, const char* file, int line)
{
    return 0;
}

void* CMemoryManager::AllocateAligned(size_t numBytes,size_t alignment,const char* file, int line)
{
    return 0;
}

void CMemoryManager::Release(void* address)
{

}

void CMemoryManager::ReleaseAligned(void* address)
{

}

const char* const CMemoryManager::GetTextLine()
{
    return 0;
}

void CMemoryManager::Shutdown()
{

}
